//
//  Item.swift
//  Grad List
//
//  Created by studentadmin on 3/6/20.
//  Copyright © 2020 studentadmin. All rights reserved.
//

import Foundation

struct Item {
  var title: String?
  var discription: String?
  var location: String?
  var dueDate: String
}
