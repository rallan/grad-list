//
//  ItemCell.swift
//  Grad List
//
//  Created by studentadmin on 3/6/20.
//  Copyright © 2020 studentadmin. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!

    var item: Item? {
      didSet {
        guard let item = item else { return }

        nameLabel.text = item.title
        discriptionLabel.text = item.discription
        locationLabel.text = item.location
        dueDateLabel.text = item.dueDate

      }
    }
    var clickedItem: Item? {
      didSet {
        guard let clickedItem = item else { return }

        nameLabel.text = clickedItem.title
        discriptionLabel.text = clickedItem.discription
        locationLabel.text = clickedItem.location
        dueDateLabel.text = clickedItem.dueDate

      }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        clickedItem = item
        // Configure the view for the selected state
    }
    
    func getCickedItem(Item : Item) -> Item  {
            setSelected(true, animated: true)
        return clickedItem!
    }
}
