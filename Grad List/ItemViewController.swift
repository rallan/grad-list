
import UIKit
class ItemsViewController: UITableViewController {
    var itemsDataSource = ItemsDataSource()
    var itemToPass : Item?

    @IBOutlet weak var AddItemBtn: UIBarButtonItem!
    

override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        if segue.identifier == "editItemSegue"{
        
        let destinationVC = segue.destination as!UINavigationController
        let destination = destinationVC.topViewController as! EditViewController
        destination.oldItem = sender as? Item
        }
   }
    
    @IBAction func EditSegue(_ sender: Any) {
        
    performSegue(withIdentifier: "editItemSegue", sender: itemToPass)
    }
}

extension ItemsViewController
  {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        {
        itemsDataSource.numberOfItems()
        }
    
    
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            let cell = tableView.dequeueReusableCell( withIdentifier: "ItemCell", for: indexPath)as! ItemCell
            cell.item = itemsDataSource.item(at: indexPath)
           // print(cell.item)
            return cell
        }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell( withIdentifier: "ItemCell", for: indexPath)as! ItemCell
        cell.item = itemsDataSource.item(at: indexPath)
        itemToPass = cell.item
       // print(cell.item)
    }
    
    
   override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
        {
        if editingStyle == .delete
            {
            self.itemsDataSource.items.remove(at: indexPath[1])
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
    }

extension ItemsViewController
{

  @IBAction func cancelToItemsViewController(_ segue: UIStoryboardSegue)
    {
    }

  @IBAction func saveItemsDetail(_ segue: UIStoryboardSegue)
    {
    print("got here")
        if segue.identifier == "SaveItemDetail" {
    guard
      let itemDetailsViewController = segue.source as? ItemDetailsViewController,
      let Item = itemDetailsViewController.item
      else {
        return
            }
    itemsDataSource.append(item: Item, to: tableView)
    itemsDataSource.sort(item:Item, to: tableView)
    }
        
    if segue.identifier == "EditItemDetail" {
       guard
         let editViewController = segue.source as? EditViewController,
         let Item = editViewController.item
         else {
           return
               }
       itemsDataSource.append(item: Item, to: tableView)
       itemsDataSource.sort(item:Item, to: tableView)
       }
        
        
    }
}
    
  



