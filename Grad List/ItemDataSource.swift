import UIKit

class ItemsDataSource: NSObject {

  var items: [Item]

  static func generateItemsData() -> [Item] {
    return [
        Item(title: "Bill Evans", discription: "Tic-Tac-Toe", location: "work", dueDate: "3/30/20 04:20" ),
        Item(title: "Oscar Peterson", discription: "Spin the Bottle", location: "home", dueDate: "3/20/20 04:20" ),
      Item(title: "Dave Brubeck", discription: "Texas Hold 'em Poker", location: "school", dueDate: "3/10/20 04:20")
    ]
  }


  override init() {
    items = ItemsDataSource.generateItemsData()

  }

 
  func numberOfItems() -> Int {
    items.count
  }

  func append(item: Item, to tableView: UITableView) {
    items.append(item)
    tableView.insertRows(at: [IndexPath(row: items.count-1, section: 0)], with: .automatic)
  }

  func item(at indexPath: IndexPath) -> Item {
    items[indexPath.row]
  }
    
    func sort(item: Item, to tableView: UITableView){
    let sortedItems = items.sorted {$0.dueDate > $1.dueDate}
    items = sortedItems
    tableView.reloadData()
    }
}

