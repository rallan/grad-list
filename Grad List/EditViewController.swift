
import UIKit

class EditViewController: UITableViewController {
      var itemsDataSource = ItemsDataSource()
      var item: Item?
      var oldItem :Item?


    @IBOutlet weak var titleTextFieldEdit: UITextField!
    @IBOutlet weak var detailLabelEdit: UITextField!
    @IBOutlet weak var locationLabelEdit: UITextField!
    
    @IBOutlet weak var datePickerEdit: UIDatePicker!
    @IBOutlet weak var dateLabelEdit: UILabel!

      @IBAction func datePickerChanged(_ sender: Any) {
          let dateFormatter = DateFormatter()
          
          dateFormatter.dateStyle = DateFormatter.Style.short
          dateFormatter.timeStyle = DateFormatter.Style.short
          
          let strDate = dateFormatter.string(from: datePickerEdit.date)
          dateLabelEdit.text = strDate
      }
      
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)  {

      if segue.identifier == "EditItemDetail"  ,
         let itemTitleEdit = titleTextFieldEdit.text,
         let itemDescriptionEdit = detailLabelEdit.text,
         let itemLocationEdit = locationLabelEdit.text,
          let itemDateEdit = dateLabelEdit.text
      {
          item = Item(title: itemTitleEdit, discription: itemDescriptionEdit, location: itemLocationEdit, dueDate: itemDateEdit)
          
          //datePicker.date) itemDate )
      }
    }
      
      
    override func viewDidLoad() {
        super.viewDidLoad()
        }
    override func viewWillAppear(_ animated: Bool) {
        titleTextFieldEdit.text = oldItem?.title
        detailLabelEdit.text = oldItem?.discription
        locationLabelEdit.text = oldItem?.location
        dateLabelEdit.text = oldItem?.dueDate


    }
    @IBAction func cancelToItemsViewController(_ segue: UIStoryboardSegue)
      {


      }
      @IBAction func saveItemDetail(_ segue: UIStoryboardSegue) {
      guard
        let itemDetailsViewController = segue.source as? ItemDetailsViewController,
        let Item = itemDetailsViewController.item
        else {
          return
              }
      itemsDataSource.append(item: Item, to: tableView)
      itemsDataSource.sort(item:Item, to: tableView)
      }
}

    
    
    
    
    
    
    

