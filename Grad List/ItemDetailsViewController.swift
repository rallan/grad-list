

import UIKit

class ItemDetailsViewController: UITableViewController {

    var item: Item?
    
  @IBOutlet weak var titleTextField: UITextField!
  @IBOutlet weak var detailLabel: UITextField!
  @IBOutlet weak var locationlabel: UITextField!
    
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var datePicker: UIDatePicker!
    
    
    @IBAction func datePickerChanged(_ sender: Any) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: datePicker.date)
        dateLabel.text = strDate
    }
    
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)  {

    if segue.identifier == "SaveItemDetail"  ,
       let itemTitle = titleTextField.text,
       let itemDescription = detailLabel.text,
       let itemLocation = locationlabel.text,
        let itemDate = dateLabel.text
    {
        item = Item(title: itemTitle, discription: itemDescription, location: itemLocation, dueDate: itemDate)
        
        //datePicker.date) itemDate )
    }
  }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}
